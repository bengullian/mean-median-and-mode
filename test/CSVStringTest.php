<?php

use PHPUnit\Framework\TestCase;

require  dirname(__DIR__)."/src/tech-test.php";

final class CSVStringTest extends TestCase 
{

    public function testReturnsValueFromCSString() 
    {
        $this->assertEquals(23, (new Wrapper())->getValueFromCSText('2310,23', 1));
    }


    public function testNumberOfRecordsParsed()
    {
        $wrapper = new Wrapper();
        $wrapper -> parseInputFile();
        $this->assertEquals(269870, sizeof($wrapper -> sort()));
    }

}