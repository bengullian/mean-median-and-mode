<?php
/*
    test Solution
    By: Benjamin Major
    Email: bengulliano@gmail.com 
    Date: 28-04-2019    
*/


require 'src/SalesTree.php';

//echo "memory usage before runing script:".round(memory_get_usage()/1048576,2).'MB';
//echo PHP_EOL;

$test = new Wrapper();
$test->parseInputFile();
$test->sort();
print_r($test->getReport());
echo PHP_EOL;

//echo "memory usage when runing script:".round(memory_get_usage()/1048576,2).'MB';
//echo PHP_EOL;

class Wrapper {

    private $salesTree;

    function __construct() {
        $this->salesTree = new SalesTree();
    }

    //  read the input file, line by line and parse
    //  each row uses the getValueFromCSText method to retrive the desired value.
    function parseInputFile() {
        $sales_file = fopen(dirname(__File__)."/testdata.csv", "r") or die("cannot open given file!"); 

        while(!feof($sales_file)) {
            $sale = $this->getValueFromCSText(fgets($sales_file), 1);
            // if there is column header of non numerical value,  it will be ignored
            if ($sale > 0) {                                
                $salesNode = new TreeNode((float)$sale);
                $this->salesTree->addNode($salesNode);                
            }
            
        }

        fclose($sales_file);

    }


    /*
    *   @Param param, @param required_column
    *   This method returns the text in the required column from a
    *   comma seperated string. 
    *   Expects to receive comma seperated text and the column number of interest as arg
    */
    function getValueFromCSText(string $param, int $required_column) {    
        $sales_value = strtok($param, ",");
        $count = 0;

        while ($count++ < $required_column) {
            $sales_value = strtok(",");        
        }   

        return $sales_value;

    }


    function sort($direction = "ascending") {
        
        return $this->salesTree->sort($direction);

    }


    function getReport() {
        
        return $this->salesTree-> getReport();

    }

}


