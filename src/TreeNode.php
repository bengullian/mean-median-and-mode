<?php

require 'SaleObject.php';

class TreeNode extends SaleObject 
{
    
    private $leftChild;
    private $rightChild;
      
    function getLeftChild() {
        return $this->leftChild;
    }


    function getRightChild() 
    {
        return $this->rightChild;
    }


    /*
     *   Adds a node to either the left or right side of a parent node. 
     *   If the new node is lower in value that its parent, it is placed to the left of its 
     *   parent, else, it is placed to the right of its parent The new node will be passed 
     *   further down the tree till it finds a suitable position    
    */
    function addChild(SaleObject $node)
    {
        if ($this->getSalePrice() < $node->getSalePrice()) {
            
            if ($this->rightChild == null) {
                $this->rightChild = $node;
            }

            else {
                return $this->rightChild->addChild($node);
            }
        }

        elseif ($this->getSalePrice() > $node->getSalePrice()) {

            if ($this->leftChild == null) {
                $this->leftChild = $node;
            }
            
            else {
                return $this->leftChild->addChild($node);
            }            
        }

        else {
            $this->addSaleEvent($node->getSalePrice());
        }
    }

    //  method that searches for and returns a sale object based of the given price
    //  @Param   salePrice      The price at which the sale was made
    function findSale(float $salePrice) : SaleObject 
    { 
        if ($salePrice == $this->getSalePrice()) {            
            return $this;
        }

        elseif ($salePrice < $this->getSalePrice() && $this->getLeftChild()) {           
            return $this->getLeftChild()->findSale($salePrice);
        }
        
        elseif ($salePrice > $this->getSalePrice() && $this->getRightChild()) {            
            return $this->getRightChild()->findSale($salePrice);
        }     

        return null;
    }

}
