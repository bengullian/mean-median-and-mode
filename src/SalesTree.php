<?php

require 'TreeNode.php';

class SalesTree
{
    private $root;
    private $salesCounted;
    private $totalSales;
    private $sortedList = array();
    private $modalSale;
    

    /*  add a node to the tree from the root
     *   if the tree has no root, the first node becomes the root
     *   else pass the node to its position in the tree
    */
    function addNode(TreeNode $node) : void 
    {
        if ($this->root == null) {
            $this->root = $node;
            $this->modalSale = $node;
        }
        
        else {
            $this->root->addChild($node);
        }
        $this->salesCounted++;
        $this->totalSales += $node->getSalePrice();

        if ($node->getSaleFrequency() > $this->modalSale->getSaleFrequency()) {
            $this->modalSale = $node; 
        }
    }


    //  returns the number of sales
    function getCount() : int 
    {
        return $this->salesCounted;
    }


    /*
     *  This method will search for a sale object and return it if exist
     *  @Param sale     The price of the sale event 
    */
    function findSale(float $sale) {
        return $this->root->findSale($sale);
    }


    /*
     *    @Param direction      string: "ascending" / "descending"
     *                          defaults to "ascending"
     *      The data structure used already sorts the data
     *      This method uses the visit method to make a list
     *      of the sorted figures 
    */
    function sort(string $direction = 'ascending') {

        $this->visit($this->root);                
       
        if ($direction == 'descending') {
            return array_reverse($this->sortedData);
        }

        return $this->sortedData;
    }



    function getReport() { 
        $stats = new Class{};       
        $stats->total = round($this->getTotalSales(), 2);
        $stats->mean = round($this->getMean(), 2);

        $stats->modal = array($this->modalSale->getSalePrice());
        $stats->frequency = $this->modalSale->getSaleFrequency();
        $stats->median = round($this->getMedianSale(), 2);    

        return json_encode($stats);
    }

    
    //  Travers the tree since and create a list of all the nodes on the tree
    private function visit($node) {
        
        if ($node->getLeftChild() != null) {
            $this->visit($node->getLeftChild());
        }

        $this->sortedData[] = $node->getSalePrice(); 
        
        // since the tree doesnt contain duplicates
        // handle cases where the frequency of a sale is higher than 1 
        if ($node->getSaleFrequency() > 1 ) {

            for ($x = 1; $x < $node->getSaleFrequency(); $x++) {
                $this->sortedData[] = $node->getSalePrice();  
            }
        }      

        if ($node->getRightChild() != null) {
            $this->visit($node->getRightChild());
        }         
    }
    

    //  returns the sum of all the sales
    private function getTotalSales() {
        return $this->totalSales;

    }


    // calculates the median value
    private function getMedianSale() {

        // if data has not be sorted, sort it
        if (sizeof($this->sortedData) == 0) {
            $this->sort();
        }

        //  using the list of sorted data, find the middle value
        $center = floor(sizeof($this->sortedData) / 2);
        $left = array_slice($this->sortedData, $center-1, 1);
        $right =  array_slice($this->sortedData, -$center, 1);
        
        return (float) ($left[0] + $right[0])/2; 
    }


    // calculate and return the mean or average sales value
    private function getMean() {
        return (float)$this->totalSales/$this->salesCounted;

    }

    
}