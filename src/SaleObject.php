<?php

class SaleObject 
{

    private $salePrice;
    private $saleFrequency;  

    public function __construct(float $price) 
    {
        $this->salePrice = $price;
        $this->saleFrequency += 1;
    }

    
    function addSaleEvent($price)
    {
        if ($price > 0 && $price == $this->salePrice) {
            $this->saleFrequency += 1;        
        }        
    
    }


    function getSalePrice() : float
    {
        return $this->salePrice;
    }


    function getSaleFrequency() : int
    {
        return $this->saleFrequency;
    }

}
